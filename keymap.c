#include QMK_KEYBOARD_H
#include <print.h>

#define DEFAULT_LAYER 0
#define FN_LAYER 1
#define NUMPAD_LAYER 2
#define CAPSLOCK_LAYER 3
#define DOFUS_LAYER 4

// Helpful defines
#define _______ KC_TRNS
#define XXXXXXX KC_NO
#define INIT_DELAY 1200

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

	[DEFAULT_LAYER] = LAYOUT_60_ansi(
    KC_GESC, KC_1, KC_2, KC_3, KC_4, KC_5, KC_6, KC_7, KC_8, KC_9, KC_0, KC_MINS, KC_EQL, KC_BSPC,
    KC_TAB, KC_Q, KC_W, KC_E, KC_R, KC_T, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_LBRC, KC_RBRC, KC_BSLS,
    MO(1), KC_A, KC_S, KC_D, KC_F, KC_G, KC_H, KC_J, KC_K, KC_L, KC_SCLN, KC_QUOT, KC_ENT,
    KC_LSFT, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_N, KC_M, KC_COMM, KC_DOT, KC_SLSH, KC_RSFT,
    KC_LCTL, KC_LGUI, KC_LALT, KC_SPC, KC_RALT, MO(1), KC_APP, KC_LCTL),

	[FN_LAYER] = LAYOUT_60_ansi(
    KC_GRV, KC_F1, KC_F2, KC_F3, KC_F4, KC_F5, KC_F6, KC_F7, KC_F8, KC_F9, KC_F10, KC_F11, KC_F12, KC_DEL,
    DF(2), DEBUG, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_PGUP, KC_UP, KC_PGDN, KC_PSCR, XXXXXXX, XXXXXXX, XXXXXXX,
    _______, KC_MUTE, KC_VOLD, KC_VOLU, XXXXXXX, XXXXXXX, KC_HOME, KC_LEFT, KC_DOWN, KC_RGHT, XXXXXXX, XXXXXXX, RESET,
    KC_LSFT, BL_TOGG, BL_DEC, BL_INC, XXXXXXX, XXXXXXX, KC_END, XXXXXXX, DF(2), DF(3), DF(4), KC_RSFT,
    KC_LCTL, KC_LGUI, KC_LALT, DF(4), KC_RALT, _______, _______, KC_RCTL),

	[NUMPAD_LAYER] = LAYOUT_60_ansi(
    XXXXXXX, KC_NLCK, KC_PSLS, KC_PAST, KC_PMNS, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_BSPC,
    DF(0), KC_P7, KC_P8, KC_P9, KC_PPLS, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_BSLS,
    XXXXXXX, KC_P4, KC_P5, KC_P6, KC_PMNS, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_ENT,
    XXXXXXX, KC_P1, KC_P2, KC_P3, KC_PEQL, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, DF(0), KC_RSFT,
    XXXXXXX, XXXXXXX, KC_P0, KC_PDOT, XXXXXXX, XXXXXXX, DF(0), KC_RCTL),

	[CAPSLOCK_LAYER] = LAYOUT_60_ansi(
    KC_ESC, KC_1, KC_2, KC_3, KC_4, KC_5, KC_6, KC_7, KC_8, KC_9, KC_0, KC_MINS, KC_EQL, KC_BSPC,
    KC_TAB, KC_Q, KC_W, KC_E, KC_R, KC_T, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_LBRC, KC_RBRC, KC_BSLS,
    KC_CAPS, KC_A, KC_S, KC_D, KC_D, KC_F, KC_H, KC_J, KC_K, KC_L, KC_SCLN, KC_QUOT, KC_ENT,
    KC_LSFT, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_N, KC_M, KC_COMM, KC_DOT, KC_SLSH, KC_RSFT,
    KC_LCTL, KC_LGUI, KC_LALT, KC_SPC, KC_LCTL, XXXXXXX, DF(0), KC_RCTL),

	[DOFUS_LAYER] = LAYOUT_60_ansi(
    KC_ESC, KC_1, KC_2, KC_3, KC_4, KC_5, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
    KC_F2, KC_6, KC_7, KC_8, KC_9, KC_0, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
    KC_F3, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
    KC_F1, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, DF(0), XXXXXXX,
    KC_LCTL, KC_LGUI, KC_LALT, DF(0), XXXXXXX, XXXXXXX, DF(0), XXXXXXX)
};

static uint16_t delay_runonce;

void rgblight_init_user(void) {
  delay_runonce = timer_read();
}

void rgblight_scan_user(void) {
  static bool runonce = true;
  if (runonce && timer_elapsed(delay_runonce) > INIT_DELAY) {
    runonce = false;
    rgblight_disable();
  }
}

void matrix_init_user(void) {
  rgblight_disable();
  rgblight_init_user();
}

void matrix_scan_user(void) {
  rgblight_scan_user();

  static uint8_t old_default_layer = 255;
  uint8_t cur_default_layer = biton32(default_layer_state);

  static uint8_t old_layer = 255;
  uint8_t cur_layer = biton32(layer_state);


  if (old_layer != cur_layer) {
    // dprintf("Layer changed\n");
    // dprintf("Default layer: %d\n", cur_default_layer);
    // dprintf("Layer: %d\n", cur_layer);
    switch (cur_layer) {
      case DEFAULT_LAYER:
        if (cur_default_layer == DEFAULT_LAYER) {
          rgblight_disable();
        }
        break;
      case FN_LAYER:
        if (cur_default_layer == DEFAULT_LAYER) {
          rgblight_enable();
          rgblight_setrgb_cyan();
        }
        break;
    }
    old_layer = cur_layer;
  }

  if (old_default_layer != cur_default_layer) {
    // dprintf("Default layer changed\n");
    // dprintf("Default layer: %d\n", cur_default_layer);
    // dprintf("Layer: %d\n", cur_layer);
    switch (cur_default_layer) {
      case DEFAULT_LAYER:
        rgblight_disable();
        break;
      case NUMPAD_LAYER:
        rgblight_enable();
        rgblight_setrgb_green();
        break;
      case CAPSLOCK_LAYER:
        rgblight_enable();
        rgblight_setrgb_red();
        break;
      case DOFUS_LAYER:
        rgblight_enable();
        rgblight_setrgb_yellow();
        break;
    }
    old_default_layer = cur_default_layer;
  }
}
